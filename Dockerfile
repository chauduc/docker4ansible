FROM centos/systemd

MAINTAINER DucCNV chauduc.1211@gmail.com

#Install repository
RUN yum -y update
RUN yum -y install epel-release
RUN yum -y localinstall http://rpms.famillecollet.com/enterprise/remi-release-7.rpm

#Add user: interdev
RUN useradd -m interdev && usermod -aG wheel interdev

#Add initscripts
RUN yum -y install initscripts && yum clean all

#Add C/C++ install
RUN yum install -y gcc

#Add wget command
RUN yum install -y wget

#Add ssh
RUN yum install -y openssh-server
RUN yum install -y openssh-clients

#Add ansible
RUN yum install -y ansible

RUN mkdir /root/.ssh/
ADD resource/id_rsa.pub /root/.ssh/id_rsa.pub
ADD resource/id_rsa /root/.ssh/id_rsa
RUN cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
RUN chmod 700 /root/.ssh/id_rsa

#Open port 
EXPOSE 22

CMD ["/usr/sbin/init"]
